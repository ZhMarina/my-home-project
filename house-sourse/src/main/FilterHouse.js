import { useHistory } from "react-router-dom"
import './index.css';


const FilterHouse = ({allHouses}) => {

    const history = useHistory();

  

    const countries = allHouses
        ? Array.from(new Set(allHouses.map((h) => h.country))) // array unique country
        : [];
    countries.unshift(null); // add empty default option

    const onSearchChange = (e) => {
        const country = e.target.value;
        history.push(`/searchresults/${country}`);
        // console.log(history.location.pathname)
    };

    // const getCountry = useCallback(
    //     (e) => {
    //         setFilterCountry(e.target.value);
    //         history.push(`/searchresults/${filterCountry}`);   
    //     }, 

    //     // [e.target.value] 
    // );


    return (
        <section className="Filter-container">
            <div> Look for your dream house in country: </div>
            {/* <select defaultValue={selected} className='Select' onChange={onSearchChange}>
                    <option value=""></option>
                    <option value="Switzerland" > Switzerland </option>
                    <option value="The Netherlands">The Netherlands </option>
                </select>  */}

            <select
                className="Select"
                value
                onChange={onSearchChange}
            >
                {countries.map(country =>
                    <option key={country} value={country}>{country}</option>
                )}
            </select>



        </section>
    )

}

export default FilterHouse