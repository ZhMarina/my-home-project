import React from 'react';
import { useState, useEffect, useMemo } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import axios from 'axios';
import './index.css';
import Header from './Header';
import FeaturedHouse from './FeatureHous';
import FilterHouse from './FilterHouse';
import SearchResult from './search/SearchResults'
import HouseFromQuery from './house/HouseFromQuery';

// GET ALL INFO ABOUT HOMES
/// a) when we have CC - use async componentDidMount await fetch

// class App extends React.Component {               
//     state = { data: [] };

//     async componentDidMount() {
//         const response = await fetch(`/houses.json`);
//         const json = await response.json();
//         this.setState({ data: json });
//     }
//     render() {
//         return <div>sdfghj</div>;
//     }
// }
// export default App;



/// b) when we have FC - use:  useEffect + async + await axios.get / await fetch

function App() {
    const [allHouses, setAllHouses] = useState([]);

    // );

    // 'I'
    useEffect(() => {
        const getAllHouses = async () => {
            const response = await axios.get("/houses.json"); // promise settles and return its result
            setAllHouses(response.data);
        };
        getAllHouses(); // Call the function
    }, []);

    // `II`
    // useEffect(() => {
    //     const getAllHouses = async () => {
    //         const response = await fetch("/houses.json"); // starts an HTTP request to houses.json
    //         const houses = await response.json();  //extract a JSON object from the response
    //         setAllHouses(houses);
    //     };
    //     getAllHouses();
    // }, []);



    const featureHouse = useMemo(() => {
        if (allHouses.length) {
            const randomIndex = Math.floor(Math.random() * allHouses.length);
            // setFeatureHouse(allHouses[randomIndex]);
            return allHouses[randomIndex]
        }
    }, [allHouses]);

    return (
        <Router>
            <div className="Container">
                <Header subtitle={'Providing houses all over the world'} />
                <FilterHouse allHouses={allHouses} />


                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/searchresults/:country" >
                        <SearchResult allHouses={allHouses} />
                    </Route>
                    <Route path="/house/:id">
                        <HouseFromQuery allHouses={allHouses} />
                    </Route>
                    <Route path="/">
                        <FeaturedHouse house={featureHouse} />
                    </Route>
                </Switch>
            </div>
        </Router>

    );
}






export default App;