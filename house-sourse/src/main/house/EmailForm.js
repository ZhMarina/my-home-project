import {useState} from "react";

const DEFAULT_CONTACT_INFO = {
    name: "",
    email: "",
    remarks: "",
}

function UseFormField({ displayStyle }) {

    const [contactInfo, setContactInfo] = useState(DEFAULT_CONTACT_INFO);

    const setInfo = (e) => {
        setContactInfo({
            ...contactInfo,
            [e.target.id]: e.target.value
        });
        console.log(e.target.value)
    };

    const submitForm = (e) => {
        e.preventDefault()
        setContactInfo(DEFAULT_CONTACT_INFO)
    }

    return (
        <form className={displayStyle ? 'contact-form-active' :'contact-form' } onSubmit={submitForm}>
            <div>
                <label> Name </label>
                <input type='name' id='name' value={contactInfo.name} onChange={setInfo} />
            </div>
            <div>
                <label> Email</label>
                <input type='email' id='email' value={contactInfo.email} onChange={setInfo} />
            </div>
            <div>
                <label> Remarks</label>
                <input type='remarks' id='remarks' value={contactInfo.remarks} onChange={setInfo} />
            </div>
            <input type='submit' text='Send' />
        </form>
    );
}


export default UseFormField