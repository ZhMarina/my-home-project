import './index.css';
import { useState } from "react";
import email from "./Email.png";
import UseFormField from './EmailForm';

const House = ({house}) => {

  const [displayStyle, setDisplayStyle] = useState(false)

  const openContactForm = () => {
    setDisplayStyle(!displayStyle)
  }



  return (
    <div >
      <div>
        <h3>{house.country} </h3>
      </div>
      <div>
        <h2>{house.address}</h2>
      </div>
      <div className='House-Info'>
        <div>
          <img src={`/images/${house.photo}.jpeg`} alt="House" />
        </div>
        <div className='House-Description'>
          <p>${house.price}</p>
          <p> Description: {house.description}</p>
          <div>
            <img onClick={openContactForm} className='email' src={email} alt="House" />
          </div>
          <div>
            <UseFormField displayStyle={displayStyle} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default House;