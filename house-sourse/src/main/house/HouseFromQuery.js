import { useParams } from "react-router-dom";
import House from './House'



const HouseFromQuery = ({ allHouses }) => {

  const {id} = useParams(); //need to make sure the {id} matches with our /house/:id parameter.
  const house = allHouses.find((h) => h.id === parseInt(id)); // === Number(id)     parseInt takes a string as an argument and returns an integer

  if (!house)
    return <div>House not found.</div>;

  return <House house={house}></House>;
}

export default HouseFromQuery;

