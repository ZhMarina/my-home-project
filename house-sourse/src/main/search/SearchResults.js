import {useParams} from "react-router-dom";
import SearchResultsList from './SearchResultsList'
import './index.css';

const SearchResult = ({ allHouses }) => {
    let {country} = useParams(); //need to make sure the { country } matches with our /searchresults/:country parameter.
    const filteredHouses = allHouses.filter((house) => house.country === country);

    if (!filteredHouses) {
        return <div>Now showing post {country} </div>
    }

    return (
        <div>

            <h2>Results for {country}:</h2>
            <div>
                {filteredHouses.map(house => (
                    <SearchResultsList house={house} />
                ))
                }
            </div>
        </div>
    )

}

export default SearchResult;

