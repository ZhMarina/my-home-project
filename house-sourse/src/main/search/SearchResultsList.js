import { useHistory } from "react-router-dom";


const SearchResultsList = ({ house }) => {
  const history = useHistory();

  const setActive = () => {
    history.push(`/house/${house.id}`);
  };

  return (
    <table>
      <tr onClick={setActive}>
        <td>{house.address}</td>
        <td>{house.price}</td>
      </tr>
    </table>

  );
};

export default SearchResultsList;
