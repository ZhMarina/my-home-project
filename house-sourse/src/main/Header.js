import React from 'react';
import GloboLogo from './GloboLogo.png';
import {Link}  from "react-router-dom";

const Header = ({subtitle}) => {
    return (
        <div className="App">
            <header className="App-header">
                <div className="Container-app-logo">
                    <Link to="/"> <img src={GloboLogo} className="App-logo" alt="logo" /> </Link>  
                </div>
                <div className='Header-name'> {subtitle} </div>
            </header>
        </div>
    )
}

export default Header 