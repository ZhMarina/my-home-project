import React from 'react';
import House from './house/House'


function FeaturedHouse({ house}) {

    if (!house) {
        return <div>No featured house at this time</div>
    }
    return (
        <section>
            <div className='Featured-house'> Featured house </div>
            <House house={house} />
        </section>

    );
}

export default FeaturedHouse;