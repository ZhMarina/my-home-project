
import React, { Component } from 'react';
import './addEvent.css'
import Select from 'react-select';
import Tooltip from 'react-tooltip'



const members = [
  {
    label: "Ivanov Ivan",
    value: "Ivanov Ivan",
  },
  {
    label: "Sorokin Petro ",
    value: "Sorokin Petro ",
  },
  {
    label: "Solovey Victoriia",
    value: "Solovey Victoriia",
  },
  {
    label: "Coval Taras ",
    value: "Coval Taras ",
  },
]


class Tags extends Component {
  render() {
    const arr = this.props.value.map(item => item)
   
    return (
      <Select
      
        styles={{
          multiValue: base => ({
            ...base,
            border: `1px dotted red`,
          }),
        }}
        value={arr}

        isMulti
        options={members}
        onChange={this.props.onChange}
      />
    )
  }
}

export default Tags















