import React, { Component } from 'react';


const LOCATION_LIST_KEY = 'LOCATION_LIST'

class Location extends React.Component {

    state = { locationList: [], text: "" }

    handleChange = (e) => {             
        this.setState({
            text: e.target.value,
        });

    };

    handleSubmit = (e) => {

        if (this.state.text.length == 0) {
            alert("write text")
            return
        }
        const newList = {
            text: this.state.text,
            id: this.state.locationList.length,
        }
        this.setState(function (state) {
            return {
                locationList: state.locationList.concat(newList),
                text: ""
            }
        }, () => {
            localStorage.setItem(LOCATION_LIST_KEY, JSON.stringify(this.state.locationList))
            this.props.onSave(this.state.locationList)
        }
        )

    }

    

    componentDidMount() {
        if (localStorage.getItem(LOCATION_LIST_KEY) !== null) {
            this.setState({
                locationList: JSON.parse(localStorage.getItem(LOCATION_LIST_KEY))
            })
        }

    }

    deleteLocationItem = (id) => {
        this.setState({
            locationList: this.state.locationList.filter((item) => item.id !== id)
        }, () => {
            localStorage.setItem(LOCATION_LIST_KEY, JSON.stringify(this.state.locationList))
            this.props.onSave(this.state.locationList)
        });
    };

    saveListApp = () => {
        this.props.onSave(this.state.locationList)
    }


    render() {

        return (
            <div className="add_location">
                <input type="text" value={this.state.text} onChange={this.handleChange} placeholder=" location name" />
                <button onClick={this.handleSubmit}>
                    Add location
                </button>
                <List allList={this.state.locationList} onDelete={this.deleteLocationItem} />
            </div>
        )
    }


}


class List extends React.Component {
    render() {
        return (
            <ol >
                {this.props.allList.map(item => (
                    <ListItem item={item} onDelete={this.props.onDelete} />
                ))}
            </ol>
        );
    }
}


class ListItem extends React.Component {

    state = { class: "Select_location_list", select: false }

    handleEdit = (e) => {
        this.props.editEvent(this.props.item.id)
    }

    handleDelete = (e) => {
        this.props.onDelete(this.props.item.id)
    }

    selectLine = (e) => {

        if (this.state.select) {
            this.setState({
                class: "Select_location_list",
                select: false
            })
            return
        }
        if (!this.state.select) {
            this.setState({
                class: "Unselect_location_list",
                select: true
            })
            return
        }
    }

    render() {
        const { item } = this.props

        return (
            <div className={"location_list"} onClick={this.selectLine}  >
                <li className={this.state.class}>
                    {item.text}
                </li>
                {this.state.select ? <div>
                    <div className="Delete" onClick={this.handleDelete}> &#10006; </div>
                </div> : null}
            </div>

        );
    }
}


export default Location