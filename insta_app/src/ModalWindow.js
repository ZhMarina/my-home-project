import React from 'react';
import like from './likes.png';
import chat from './chat-bubbles.png'




class ModalWindow extends React.Component {

    constructor(props) {
        super(props);
        this.textInput = React.createRef(); // создадим реф в поле `textInput` для хранения DOM-элемента
        this.state = { show: true, comment: '', allComment: [], id: 0 }
    }

    state = { show: true, comment: '', allComment: [], id: 0 }

  
    saveComment = (e) => {         //get comment value in state.comment
        this.setState({
            comment: e.target.value,
        })
    }

    focusTextInput = (e) => {  // Установим фокус на текстовое поле с помощью чистого DOM API
        this.textInput.current.focus();
    }

    sentCommentValue = (e) => {        // set comment in allComent and  onSaveComments (Insta component)
       if(this.state.comment.length ) {
        this.setState(function (state) {

            return {
                allComment: state.allComment.concat(this.state.comment),
                comment: '',
            }
        }, () => this.props.onSaveComments(this.state.allComment, this.state.id)
        )
       }
        
    }
    
    // sentCommentValue = (e) => {        // set comment in allComent and  onSaveComments (Insta component)
    //     this.setState(function (state) {
    //         return {
    //             allComment: state.allComment.concat(this.state.comment),
    //             comment: '',
    //         }
    //     }, () => this.props.onSaveComments(this.state.allComment, this.state.id)
    //     )
    // }


    deleteCommentItem = (elem) => {
        this.setState({
            allComment: this.state.allComment.filter(item => item !== elem)
        }, () => this.props.onSaveComments(this.state.allComment, this.state.id)
        )
    };


    componentDidMount() {
        this.setState({
            id: this.props.onModalWindowShow.id,   // set necessary id 
        }, () => {
            this.props.photoList.map((item) => {     // get and set alls comment for this post 
                if (item.id === this.state.id) {
                    let info = item.comment
                    this.setState(function (state) {
                        return {
                            allComment: info
                        }
                    })
                }
            })
        })
    }





    render() {
        const { onModalWindowShow } = this.props

        const itemsToRender = this.state.allComment.map((item) =>
            <ListItem item={item} onDelete={this.deleteCommentItem} />
        )

        return (
            <div className="Open_modal">
                <img className="Image_photo_modal" src={onModalWindowShow.thumbUrl} />
                <section className="Modal_description">
                    <div className="Modal_header">
                        <img className="photo_header_modal" title='Change photo' src="https://images.ua.prom.st/2326183448_w640_h640_2326183448.jpg" />
                        <div className='name_profile_modal'> <b> Borenka </b> </div>
                        <button className="Hide" onClick={this.props.onHide}> X </button>
                    </div>
                    <hr></hr>
                    <div className="Modal_info_photo">
                        <img className="photo_header_modal" title='Change photo' src="https://images.ua.prom.st/2326183448_w640_h640_2326183448.jpg" />
                        <div className='name_profile_modal2'> <b>Borenka </b> {onModalWindowShow.name} </div>
                        {/* {this.state.allComment.length > 0 ? itemsToRender : null} */}
                        <ul>
                            {itemsToRender}
                        </ul>

                    </div>
                    <hr></hr>
                    <div>
                        <img className="like" src={like} />
                        <img className="comment" title='Change photo' onClick={this.focusTextInput} src={chat} />

                    </div>

                    <div className="area_comment" >
                        <textarea ref={this.textInput} value={this.state.comment} onChange={this.saveComment} rows="3" className="give_comment" aria-label="Добавьте комментарий..." placeholder="Добавьте комментарий..." ></textarea>
                        <button className="sent" type="submit" disabled="" onClick={() => this.sentCommentValue()}> Опубликовать</button>
                    </div>
                </section>
            </div>


        )

    }
}
export default ModalWindow

class ListItem extends React.Component {

    state = { select: false }

    selectComment = (e) => {

        if (this.state.select) {
            this.setState({
                select: false
            })
            return
        }
        if (!this.state.select) {
            this.setState({
                select: true
            })
            return
        }
    }

    handleDelete = (e) => {
        this.props.onDelete(this.props.item)
    }

    render() {
        const { item } = this.props

        return (
            <section className="commentItem" >
                <li onClick={this.selectComment} className="comment_style"> {item} </li>
                { this.state.select ?
                    <p onClick={() => this.handleDelete()}>  &#9746; </p> : null
                }
            </section>
        )
    }
}
