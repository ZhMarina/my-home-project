
import { Component } from 'react';
import './App.css';
import Modal from './Modal';
import ModalWindow from './ModalWindow';

const PHOTO_LIST = [
  { name: 'GOOD Day for job ', id: 1, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/Xiaowa-Vacuum-Cleaner-C10-White-06_20022_1524654768.jpg', comment: [] },
  { name: 'I work without interruption', id: 2, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/xiaomi-roborock-vacuum-cleaner-s6-pure-white-s602-00-0010343081590413505.jpg', comment: [] },
  { name: 'walking with my dog', id: 3, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/pvm_Xiaowa-Vacuum-Cleaner-C10-White-05_20022_1524654768.jpg', comment: [] },
  { name: 'parkour', id: 4, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/Xiaowa-Vacuum-Cleaner-C10-White-12_20022_1524654822.jpg', comment: [] },
  { name: 'any obstacles will only help me become better', id: 5, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/Xiaowa-Vacuum-Cleaner-C10-White-10_20022_1524654768.jpg', comment: [] },
  { name: 'I аm old enough to leave my home', id: 6, thumbUrl: 'https://i.xiaomi.ua/u/CatalogueImage/Xiaowa-Vacuum-Cleaner-C10-White-04_20022_1524654767.jpg', comment: [] },
]

const SAVE_PHOTO_LIST = 'SAVE_PHOTO_LIST'
const SAVE_CHANGE_NAME = 'SAVE_CHANGE_NAME'
const SAVE_CHANGE_ABOUT = 'SAVE_CHANGE_ABOUT'

class InstaReact extends Component {

  state = {
    photoList: PHOTO_LIST,
    showModal: false,
    modalWindow: [],
  }

  getModal = (id) => {        // open modal window 
    let modalInfo = []
    this.setState({
      photoList: this.state.photoList.map(function (item) {
        if (id === item.id) {
          modalInfo = item
          return modalInfo
        }
        return item
      }),
      modalWindow: modalInfo,  // sent all info about necessery image into modal window
      showModal: true,
    });
  };


  hideModal = () => {             // hide modal window
    this.setState({ showModal: false });
  };

  saveComments = (arr, id) => {        // send comment from  Child component in InstaReact state
    this.setState({
      photoList: this.state.photoList.map(function (item) {   // overwrite object
        if (id === item.id) {
          const newItem = {
            ...item,
            comment: arr
          }
          return newItem
        }
        return item
      }),
    }, () => {
      localStorage.setItem(SAVE_PHOTO_LIST, JSON.stringify(this.state.photoList))    // need to save new comments
    })
  }



  componentDidMount() {
    if (localStorage.getItem(SAVE_PHOTO_LIST) !== null) {   // info + all comments
      this.setState({
        photoList: JSON.parse(localStorage.getItem(SAVE_PHOTO_LIST))
      })
    }
  }


  render() {
    const modal = this.state.showModal ? (      // modal window
      <Modal  >
        <ModalWindow photoList={this.state.photoList} onModalWindowShow={this.state.modalWindow} onHide={this.hideModal} onSaveComments={this.saveComments} />
      </Modal>
    ) : null

    return (
      <div className="container">
        <div >
          {modal}
        </div>
        <PhotoHeader />
        <hr></hr>
        <div className='photo_galer'>
          <PhotoList onShow={this.getModal} allPhotoList={this.state.photoList} />
        </div>
      </div>

    )
  }
}


class About extends Component {
 
  state = {
    profileName: 'Borenka',
    list: 'usual boy',
    edit: false
  }

  componentDidMount() {
    if (localStorage.getItem(SAVE_CHANGE_NAME) !== null && SAVE_CHANGE_ABOUT !== null) {
      this.setState({
        profileName: JSON.parse(localStorage.getItem(SAVE_CHANGE_NAME)),
        list: JSON.parse(localStorage.getItem(SAVE_CHANGE_ABOUT))
      })
    }
  }

  handleChangeNameProfile = (e) => { // записываем значение input
    
    this.setState({
      profileName: e.target.value,
    });

  };

  handleChangeAboutProfile = (e) => { // записываем значение input
    this.setState({
      list: e.target.value,
    });

  };
  
  handleSubmit = (e) => {
    this.setState({
      edit: true
    });
  }


  saveChange = (e) => {
    this.setState({
      
      edit: false
    },
      () => {
        localStorage.setItem(SAVE_CHANGE_NAME, JSON.stringify(this.state.profileName))
        localStorage.setItem(SAVE_CHANGE_ABOUT, JSON.stringify(this.state.list))
      }
    )
  }


  render() {
    return (
      <div className='allInfoProfile'>
        <img className="photo_header" title='Change photo' src="https://images.ua.prom.st/2326183448_w640_h640_2326183448.jpg" />
      <section>
        <div className="aboutProfile">
          <div className='name_profile'> {this.state.profileName} </div>
          <div className='about'> {this.state.list}</div>
        

        {
          this.state.edit ?
            <div className='edit_input'> 
              <input type="text" value={this.state.profileName} onChange={this.handleChangeNameProfile} />
              <input type="text" value={this.state.list} onChange={this.handleChangeAboutProfile} />
              <button onClick={this.saveChange}>
                Save change
              </button>
            </div> : null
        }
        </div>
      </section>
      <button className="profile_edit" onClick={this.handleSubmit}> Редактировать профиль </button>
      </div>
    )
  }
}




function PhotoHeader() {         // photo profile  + name profile 
  return (
        <About />
  )
}
// onClick={() => this.}

// const PhotoHeader = () => {
// function PhotoHeader() {         // photo profile  + name profile 
//   return (
//     <div className='photo_header_container'>
//       <img className="photo_header" title='Change photo' src="https://images.ua.prom.st/2326183448_w640_h640_2326183448.jpg" />
//       <div className='name_profile'> Borenka
//         <div className='about'>
//           <p>-I am robot</p>
//           <p>-Love cleaning</p>
//           <p>-I have a friend Garik </p>  
//         </div>
//        </div>
//       <button className="profile_edit" onClick={() => this.}> Редактировать профиль </button>
//     </div>
//   )
// }

const PhotoList = ({ allPhotoList, onShow }) => {   // all photo (post) profile
  return (
    allPhotoList.map(item =>
      <PhotoItem item={item} onShow={onShow} />)
  )
}

const PhotoItem = ({ item, onShow }) => {  // one element (post)
  return (
    <img className="photo_item" src={item.thumbUrl} onClick={() => onShow(item.id)} />
  )
}




export default InstaReact;
