
import React, { Component } from 'react';
import ModalAddEvent from './ModalAddEvent'
import AddEvent from './AddEvent'



class ListItem extends Component {

  state = {class: "Select_false", select: false}


  handleEdit = (e) => {
    this.props.editEvent(this.props.item.id)
  }

  handleDelete = (e) => {
    this.props.onDelete(this.props.item.id)
  }

  selectLine = (e) => {

    if (this.state.select) {
      this.setState({
        class: "Select_false",
        select: false
      })
      return
    }

    if (!this.state.select) {
      this.setState({
        class: "Select_true" ,
        select: true
      })
      return
    }
  }






  render() {
    const { item, order } = this.props

    return (
      <>
        <tr key={item.id} className={this.state.class} onClick={this.selectLine} >
          <td className="number" >{order}.</td>
          <td>{item.names}</td>
          <td>{item.description}</td>
          <td>{item.dataStart}</td>
          <td>{item.dataEnd}</td>
          <td>{item.importance}</td>
          <td>{item.status}</td>
          <td className="participants">{item.participants.map(item =>
            <div> {item.label} </div>)}
          </td>
          <td>{item.location.text}</td>
          {this.state.select ?
            <td className="select_delete_edit">
              <div className="Edit" onClick={this.handleEdit}> &#128394; </div>
              <div className="Delete" onClick={this.handleDelete}> &#10006; </div>
            </td> : null}
        </tr>


      </>


    )
  }
}




const EVENT_LIST_ACCESS_KEY = 'EVENTS_LIST'


class Events extends Component {

  state = { eventList: [], addEvent: false, eventEditId: 0, eventEdit: null }

  getModal = (e) => {
    this.setState({ addEvent: true });
  };

  hideModal = (e) => {
    this.setState({ addEvent: false, eventEditId: 0, eventEdit: null  });
  };


  setNewEvent = (newItem) => {
    if (newItem.names === undefined) {
      return
    }

    if (this.state.eventList.length === 0 || newItem.id > this.state.eventList.length) {

      this.setState(function (state) {
        const newEvent = {
          ...newItem,
          id: this.state.eventList.length + 1,

        }

        return {
          eventList: state.eventList.concat(newEvent),
          eventEdit: null,
        }

      }, () => { // callback ф-ия  гарантированно вызывается после того как было применено обновление и получили новое 
        localStorage.setItem(EVENT_LIST_ACCESS_KEY, JSON.stringify(this.state.eventList))
      })

      // let local = JSON.stringify(newItem)
      // localStorage.setItem(newItem.id, local)
      return
    }


    this.setState({
      eventList: this.state.eventList.map(function (item) {
        if (newItem.id === item.id) {
          return newItem
        }
        return item


      }),
      eventEdit: null

    }, () => {
      localStorage.setItem(EVENT_LIST_ACCESS_KEY, JSON.stringify(this.state.eventList))
    })
  }


  editEventItem = (id) => {
    this.setState({
      eventEditId: id,
      eventEdit: this.state.eventList.find(item => item.id === id)
    })
  }

  deleteEventItem = (id) => {
    this.setState({

      eventList: this.state.eventList.filter((item) => item.id !== id)
    }, () => {
      localStorage.setItem(EVENT_LIST_ACCESS_KEY, JSON.stringify(this.state.eventList))
    });

    // for (let key in localStorage) {
    //   if (id == key) {
    //     localStorage.removeItem(key)
    //   }
    // }
  };



  componentDidMount() {
    if (localStorage.getItem(EVENT_LIST_ACCESS_KEY) !== null){
      this.setState({
        eventList: JSON.parse(localStorage.getItem(EVENT_LIST_ACCESS_KEY))
      })
    }
    // if (localStorage.length > 0) {
    //   let values = Object.values(localStorage)

    //   values.map(value => {
    //     let returnObj = JSON.parse(value)
    //     this.setState(function (state) {
    //       return {
    //         eventList: state.eventList.concat(returnObj)

    //       }
    //     })

    //   })


    // }
  }


  render() {

    const modal = this.state.addEvent || this.state.eventEditId !== 0 ? (
      <ModalAddEvent >
        <AddEvent locationList={this.props.locationList} eventEdit={this.state.eventEdit} list={this.state.eventList} onEdit={this.editEventItem} onSetNewEvent={this.setNewEvent} onHide={this.hideModal} />
      </ModalAddEvent>
    ) : null



    const itemsToRender = this.state.eventList.map((item, index) =>
      <ListItem item={item}
        order={index + 1}
        editEvent={this.editEventItem}
        onDelete={this.deleteEventItem}
        openEvent={this.getModal}
      />)

    return (
      <div>
        <div >
          {modal}
        </div>


        {
          itemsToRender.length ?
            <div>
              <table>
                <caption className='header_table'>Events </caption>
                <thead>
                  <tr className="header">
                    <th className="number">  № </th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Begin</th>
                    <th>End</th>
                    <th>Importance</th>
                    <th>Status</th>
                    <th>Participants</th>
                    <th>Location</th>

                  </tr>
                </thead>
                <tbody>
                  {itemsToRender}
                </tbody>
              </table>

            </div> : null
        }
        <button onClick={this.getModal} className="add_event"> Add event </button>


      </div >
    )
  }
}

export default Events

