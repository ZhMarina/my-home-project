import React from 'react';
import './addEvent.css'
import Tags from './Colleagues'
import LocationSelect from './LocationSelect'



class AddEvent extends React.Component {

    state = {
        names: '',
        description: '',
        dataStart: new Date().toISOString().substr(0, 16),
        dataEnd: new Date().toISOString().substr(0, 16),
        importance: 'Medium',
        status: 'ended',
        participants: [],
        id: this.props.list.length + 1,
        location: []

    }

    openEditWindow = (props) => {
        if (this.props.eventEdit === null) {
            return
        }

        this.setState(function (state) {
            return {
                names: this.props.eventEdit.names,
                description: this.props.eventEdit.description,
                dataStart: this.props.eventEdit.dataStart,
                dataEnd: this.props.eventEdit.dataEnd,
                importance: this.props.eventEdit.importance,
                status: this.props.eventEdit.status,
                participants: this.props.eventEdit.participants,
                location: this.props.eventEdit.location,
                id: this.props.eventEdit.id,
            }
        });

    }



    componentDidMount() {

        // this.props.onEdit()
        this.openEditWindow(this.props.eventEdit)
    }

    handleParticipants = (e) => {

        this.setState(function (state) {
            return {
                participants: e
            }
        });

    }

    handleChangeLocation = (e) => {
        this.setState(function (state) {

            return {
                location: e

            }
        });
    }

    handleChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });

    }

    handleClick = (e) => {
        const newItem = {
            names: this.state.names,
            description: this.state.description,
            dataStart: this.state.dataStart,
            dataEnd: this.state.dataEnd,
            importance: this.state.importance,
            status: this.state.status,
            id: this.state.id,
            participants: this.state.participants,
            location: this.state.location
        }

        this.props.onSetNewEvent(newItem)
        this.props.onHide()

    }



    render() {
        return (

            <div className="Open_modal" >
                <div className="header_modal">
                    <h2 className="Name_event"> EVENT </h2>
                    <button className="Hide" onClick={this.props.onHide}> X </button>
                </div>
                <p>
                    <label> Name</label>
                    <textarea rows="2" cols="50" type="text" value={this.state.names} name='names' onChange={this.handleChange} required placeholder="name" />
                </p>
                <p>
                    <label> Description </label>
                    <textarea rows="4" cols="50" type="text" class="mytext" value={this.state.description} name='description' onChange={this.handleChange} placeholder="description" />
                </p>
                <p>
                    <label> Data start </label>
                    <form noValidate>
                        <input name='dataStart' onChange={this.handleChange} type="datetime-local" value={this.state.dataStart} InputLabelProps={{ shrink: true, }} />
                    </form>
                    {/* <DateAndTimePickers name='dataStart' value={this.state.dataStart} onChange={this.handleChange}  /> */}
                </p>

                <p>
                    <label> Data end </label>
                    <input required name='dataEnd' onChange={this.handleChange} type="datetime-local" value={this.state.dataEnd} InputLabelProps={{ shrink: true, }} />
                    {/* <DateAndTimePickers name='dataEnd' value={this.state.dataEnd} onChange={this.handleChange} /> */}

                </p>


                <p>
                    <label> Event participants </label>
  
                    <Tags name='participants ' onChange={this.handleParticipants} value={this.state.participants} />

                </p>

                <p>
                    <label>  Location </label>
                    <LocationSelect name='location ' onChange={this.handleChangeLocation} value={this.state.location} locationList={this.props.locationList} />
                    
                </p>



                <p>
                    <label> Importance</label>
                    <select name='importance' value={this.state.importance} onChange={this.handleChange}  >
                        <option> Low </option>
                        <option> Medium </option>
                        <option> High </option>
                    </select>

                </p >

                <p>
                    <label> Status </label>
                    <select name='status' value={this.state.status} onChange={this.handleChange}  >
                        <option > Еnded</option>
                        <option > Expected</option>
                        <option> Postponed </option>
                    </select>

                </p>

                <button  className="buttom_save" onClick={this.handleClick}>
                    Save
                </button>


            </div >




        )
    }
}

export default AddEvent

