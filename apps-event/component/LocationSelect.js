import React, { Component } from 'react';
import Select from 'react-select';





class LocationSelect extends Component {

    render() {


        const listLoc = this.props.locationList.map(function (el) { //массив всех доступных локаций
            el.label = el.text;
            return el;
        });



        return (

            <Select
                value={this.props.value}
                onChange={this.props.onChange}
                name="location"
                options={listLoc}
                className="basic-multi-select"
                classNamePrefix="select"
            />

           


        )
    }

}

export default LocationSelect 
