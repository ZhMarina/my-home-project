import React, { Component } from 'react';
import {Route, Switch, NavLink, Redirect, withRouter} from "react-router-dom"

import Events from './component/Events'
import Location from './component/Location'
import Notification from './component/Notification'



const SAVE_LOCATION_LIST = 'SAVE_LOCATION_LIST'

class App extends Component {

  state = {
    saveLocationList: null
  }

  handleSaveLocationList = (arr) => {     // write a list of locations from "Location"
    this.setState({
      saveLocationList: arr
    }, () => {
      localStorage.setItem(SAVE_LOCATION_LIST, JSON.stringify(this.state.saveLocationList))
    })
  }

  handleLocationFromAddEvent = (e) => {
        
  }





  componentDidMount() {   // at start, write a list of locations from localStorage

    if (localStorage.getItem(SAVE_LOCATION_LIST) !== null) {
      this.setState({
        saveLocationList: JSON.parse(localStorage.getItem(SAVE_LOCATION_LIST))
      })
    }

  }


  render() {
    const { history } = this.props


    return (

      <div className="App" >
        <section className="Tool ">
          <div className="Tools_button">
            <div><NavLink to='/events' activeClassName='active'> Events </NavLink></div>
            <div><NavLink to='/location' activeClassName='active'> Location</NavLink></div>
            <div><NavLink to='/notification' activeClassName='active'> Notification</NavLink></div>
          </div>


        </section>
        <main>
          <Switch>
            <Route history={history} path='/events' component={(props) => <Events {...props} locationList={this.state.saveLocationList} />} />
            <Route
              history={history}
              path='/location'
              component={(props) => <Location {...props} onSave={this.handleSaveLocationList} />}
            />
            <Route history={history} path='/notification' component={Notification} />
            <Redirect from='/' to='/events' />
          </Switch>

        </main>

      </div>
    );
  }
}



export default withRouter(App)
