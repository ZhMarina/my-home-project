import React from 'react';



class ListItem extends React.Component {

    state = {
        text: "this.props.text",
        hover: false,
        // id: 1,
        // favorite: false

    }



    changeBackground = (e) => {
        this.setState({
            hover: true,
        })
        
    }


    changeFalse = (e) => {

        this.setState({
            hover: false,

        });



    }

    render() {
        const { item } = this.props
        return (
            <section className="Film" >
                <div className="Gallery"  onMouseOver={this.changeBackground} onMouseLeave={this.changeFalse}>

                
                   


                    {this.state.hover ?
                        <section className="AllsHover"  >
                             <img className="Film_cover"  src={item.thumbUrl} />
                            <div className='Favorite_star' onClick={() => this.props.onFavoriteChange(this.props.item.id)} >
                                {this.props.item.favorite ?
                                    <img className='Star' src='https://v1.iconsearch.ru/uploads/icons/nuove/128x128/knewstuff.png' /> :
                                    <img className='Star' src='https://v1.iconsearch.ru/uploads/icons/token/128x128/star-favorites.png' />
                                }
                            </div>


                            <div className='HoverImg'>

                                <p className="Rating"> &#8795; {item.rating} </p>
                                <hr align="center" width="200" size="2" color="#ff0000" />
                                <p> {item.length} </p>
                                <p> {item.genre} </p>

                            </div>
                        </section>
                        :  <img className="Film_cover"  src={item.thumbUrl} />

                    }

                 

                </div>
                <p  > {this.props.item.name} </p>

            </section>


        )
    }
}

export default ListItem
