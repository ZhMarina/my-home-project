import React from 'react';
// import firthFilm from './photo/abs.jpeg'
import ListItem from './List'
import './App.css'


const INITIAL_MOVIES_LIST = [
    { name: 'Просто помиловать', id: 1, genre: 'Detective', length: '108 хв', rating: 6, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage33/contents/7/a/24b596a1742a4e86374e14a021456e.jpg/234x360/' },
    { name: 'Просто', length: '120 хв', id: 2, genre: 'Detective', rating: 5, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage32/contents/b/e/0a2783d55c426cdb57149b9d2b5ff6.jpg/234x360/' },
    { name: 'После', length: '92 хв', id: 3, genre: 'Detective', rating: 8.1, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage38/contents/6/9/436fa4eaf6a7c71435f84a11e3791e.jpg/234x360/' },
    { name: 'Час истины', favorite: false, length: '118 хв', id: 4, genre: 'Detective', rating: 7, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage32/contents/7/1/e79577dab581e2bf4dbf8d7362c7bf.jpg/234x360/' },
    { name: 'Зомбиленд', length: '112 хв', id: 5, genre: 'Detective', rating: 3, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage15/contents/0/c/2751fb757c3172d3533b1dd40ae7cd.jpg/234x360/' },
    { name: 'Убийства по открыткам', id: 6, genre: 'Detective', length: '88 хв', rating: 5.34, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage6/contents/f/4/d5178be46b3793ab4db1c9c6ab18c6.jpg/234x360/' },
    { name: 'Белоснежка', length: '78 хв', id: 7, genre: 'Detective', rating: 5, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage9/contents/6/c/855572f8925fe5d3f4161c53ae10a6.jpg/234x360/' },
    { name: 'Черное и синее', length: '68 хв', id: 8, genre: 'Detective', rating: 8.54, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage28/contents/c/c/87d45a6c5ac7ee69f516a8d372a5a6.jpg/234x360/' },
    { name: 'Темные воды', length: '75 хв', id: 9, genre: 'Detective', rating: 7.5, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage23/contents/6/4/527e01830a91c5075896e2b22d9e94.jpg/234x360/' },
    { name: 'Однажды в Голливуде', id: 10, length: '98 хв', genre: 'Detective', rating: 9.5, favorite: false, thumbUrl: 'https://thumbs.dfs.ivi.ru/storage31/contents/8/e/010ea0147875e28b91ef1ff01cd042.jpg/234x360/' }
]





class Gallery extends React.Component {

    state = { list: INITIAL_MOVIES_LIST, favoriteShow: false, sortingValue: "rating" }

    favoriteChange = (id) => {
        this.setState({
            list: this.state.list.map(function (item) {
                if (id === item.id) {
                    if (item.favorite === false) {
                        const newItem = {
                            ...item,
                            favorite: true,
                        }
                        return newItem
                    }
                    if (item.favorite === true) {
                        const newItem = {
                            ...item,
                            favorite: false
                        }
                        return newItem
                    }
                }
                return item

            })
        })
    }

    applySorting = (e) => {
        this.setState({
            sortingValue: e.target.value
        })

    }





    handleSorting = (e) => {

        if (this.state.sortingValue === 'rating') {
            this.setState({
                list: this.state.list.sort(function (a, b) {
                    return b.rating - a.rating
                })
            })
        }

        if (this.state.sortingValue === 'name_asc') {
            this.setState({
                list: this.state.list.sort(function (a, b) {
                    return a.name.localeCompare(b.name);
                })
            })
        }
        if (this.state.sortingValue === 'name_desc') {
            this.setState({
                list: this.state.list.sort(function (a, b) {
                    return b.name.localeCompare(a.name);
                })
            })
        }


    }













    // handleSorting = (e) => {
    //     this.setState({
    //         list: this.state.list.sort(function (a, b) {

    //             if (e.target.value === 'rating') {
    //                 return b.rating - a.rating
    //             }

    //             let sorting_value = 0;
    //             if (a.name > b.name) {
    //                 sorting_value = 1;
    //             }
    //             if (a.name < b.name) {
    //                 sorting_value = -1;
    //             }
    //             if (e.target.value === 'name_desc') {
    //                 sorting_value *= -1
    //             }

    //             return sorting_value

    //         })

    //     })
    // }





    componentDidMount() {
        this.handleSorting()
        
    };


    componentDidUpdate(prevProps, prevState) {
        if (prevState.sortingValue !== this.state.sortingValue) {
           this.handleSorting()  
        }
    }

    handleSortingVavorite = (e) => {

        if (e.target.checked === true) {
            this.setState({
                favoriteShow: true
            })
            return
        }
        return this.setState({
            favoriteShow: false
        })
    }


    render() {


        return (
            <div>
                <section className="AllSorting">
                    <div className="Sorting">
                        <div>
                            <span className="Show_favorite" > &#926; Sorting: </span>
                            <select onChange={this.applySorting} >
                                <option value="name_asc"  >A -> Z</option>
                                <option value="name_desc" > Z -> A</option>
                                <option value="rating" selected="selected"> Rating</option>
                            </select>
                        </div>
                        <form className="Show_favorite" onChange={this.handleSortingVavorite}>
                            <label for="Receipt">Show favorite</label>
                            <input type="checkbox" name="Receipt" id="Receipt" value="Yes"></input>
                        </form>
                    </div>

                </section>
                <div>
                    <List favoriteShow={this.state.favoriteShow} allList={this.state.list} onFavoriteChange={this.favoriteChange} />
                </div>
            </div >


        )
    }
}

// class List extends React.Component {
//     render() {
//         let filterList = this.props.allList.filter(item => item.favorite)
//         console.log(filterList)
//         return (

//             <ul className="App">
//                 {!this.props.favoriteShow ?
//                     this.props.allList.map(item =>
//                         (
//                             <ListItem item={item} key={item.id} onFavoriteChange={this.props.onFavoriteChange} className="ListItems" />
//                         )
//                     ) :

//                     filterList.length > 0 ? filterList.map(item =>

//                         (
//                             <ListItem item={item} key={item.id} onFavoriteChange={this.props.onFavoriteChange} className="ListItems" />
//                         )) :
//                         <div className="Not_found"> <p>No favorites film found</p><img width="200" src="https://www.pngkey.com/png/full/27-270933_oops-speech-bubble-png-oops-speech-bubble-badge.png" />  </div >
//                 }

//             </ul>



//         );
//     }
// }

const List = ({favoriteShow, allList, onFavoriteChange}) => {

    const itemsToRender = allList.filter(item => !favoriteShow || item.favorite).map(item =>
        <ListItem item={item} key={item.id} onFavoriteChange={onFavoriteChange} className="ListItems" />
    )  

    return (
        <ul className="App">
            {
                itemsToRender.length ? 
                    itemsToRender :
                    <div className="Not_found"> 
                        <p>No{favoriteShow ? ' favorites ' : ''}film found</p>
                        <img width="200" src="https://www.pngkey.com/png/full/27-270933_oops-speech-bubble-png-oops-speech-bubble-badge.png" />  
                    </div >
            }
    
        </ul>
    )
}

export default Gallery