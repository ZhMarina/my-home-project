import React from 'react';



class ListItem extends React.Component {

    state = {
        value: this.props.item.id,
        hover: false,
    }

    changeBackground = (e) => {
        this.setState({
            hover: true,
        })
    }


    changeFalse = (e) => {
        this.setState({
            hover: false,
        });
    }

    handleClick = (e) => {
        this.props.onShow(this.props.item.id)
        e.stopPropagation()
    }

    render() {
        const { item } = this.props
        

        return (
                <section className="Film"  >
                    <div className="Gallery" onMouseOver={this.changeBackground} onMouseLeave={this.changeFalse}>
                        {item.favorite && !this.state.hover ?
                            <div>
                                <img className="Film_cover_favorite" src={item.thumbUrl} />
                                <img className='Star_active' src='https://v1.iconsearch.ru/uploads/icons/nuove/128x128/knewstuff.png' />

                            </div> :
                            <img className="Film_cover" src={item.thumbUrl} onClick={this.handleClick} />
                        }

                        {this.state.hover ?
                            <section className="AllsHover"  >
                                <div className='Favorite_star' onClick={() => this.props.onFavoriteChange(this.props.item.id)} >
                                    {this.props.item.favorite ?
                                        <img className='Star' src='https://v1.iconsearch.ru/uploads/icons/nuove/128x128/knewstuff.png' /> :
                                        <img className='Star' src='https://v1.iconsearch.ru/uploads/icons/token/128x128/star-favorites.png' />
                                    }
                                </div>
                                <div className='HoverImg' onClick={this.handleClick}>
                                    <p className="Rating"> &#8795; {item.rating} </p>
                                    <hr align="center" width="200" size="2" color="#ff0000" />
                                    <p> {item.length} </p>
                                    <p> {item.genre} </p>
                                </div>
                            </section>
                            : null
                        }
                    </div>
                    <p  onClick={() =>this.props.onShow}> {this.props.item.name} </p>
                </section> 
        )
    }
}

export default ListItem
