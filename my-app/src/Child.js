import React from 'react';


class Child extends React.Component {

    state = { class: "Info", text: "Show all", show: true}


    getSizeInfo = (e) => {
        
        if (this.state.show ) {
            this.setState({
                class: "Info_1",
                text: "Hide all",
                show: false
            })
            return
        }
        if (!this.state.show ) {
            this.setState({
                class: "Info",
                text: "Show all",
                show: true
            })
            return
        }
        


    }


    render() {
        const { onModalWindowShow } = this.props
        return (
            <div className="Open_modal"> 
                <div className="header_modal"> 
                     <p className="Name_film_modal"> {onModalWindowShow.name}</p>
                     <button className="Hide" onClick={this.props.onHide}> X </button>
                </div>
                
                <section className="Film_information">
                    <div className="Film_photo_modal">

                        <img className="Image_photo_modal" src={onModalWindowShow.thumbUrl} alt="Пример" />
                    </div>
                    <div className="About_film_modal">
                        <div className="genre"> Genre: {onModalWindowShow.genre}</div>
                        <p> Rating: {onModalWindowShow.rating}</p>
                        <p>Length: {onModalWindowShow.length}</p>
                        <hr align="center" width="400" size="2" color="#ff0000" />
                        <p className="About_film"> About film: </p>
                        <div >
                            <div id="id" className={this.state.class}> {onModalWindowShow.info} </div>
                            <div className="label" onClick={this.getSizeInfo} > {this.state.text}   </div>
                        </div>
                    </div>




                </section>
            </div>

        )

    }
}

export default Child